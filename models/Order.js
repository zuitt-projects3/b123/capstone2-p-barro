const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  totalAmount: {
    type: Number,
    required: [true, "Total Amount is required"]
  },
  datePurchased: {
    type: Date,
    default: new Date()
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, "User ID is Required"],
    ref: "User"
  },
  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, "Product Id is Required"],
        ref: "Product"
      },
      name: {
        type: String,
        required: [true, "Product Name is Required"]
      },
      quantity: {
        type: Number,
        required: [true, "Quantity of Product Required"]
      },
      price: {
        type: String,
        required: [true, "Price Required"]
      }
    }
  ]
})

module.exports = mongoose.model("Order",orderSchema)
