const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');
const app = express();
const port = process.env.PORT || 4000;



app.use(express.json());

app.use(cors())

// user Routes

const userRoutes = require('./routes/userRoutes');
console.log(userRoutes);
app.use('/users', userRoutes)

// product Routes
const productRoutes = require('./routes/productRoutes');
console.log(productRoutes);
app.use('/products', productRoutes)

// order routes
const orderRoutes = require('./routes/orderRoutes');
console.log(orderRoutes);
app.use('/orders', orderRoutes)

//
mongoose.connect("mongodb+srv://princebarro:Arnaments123@cluster0.lw2dr.mongodb.net/e-commerceApi?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

app.use(express.json());
app.listen(port, ()=>console.log(`Server running at port ${port}`))
