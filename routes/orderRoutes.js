const express = require('express');
const router = express.Router();
const auth = require('../auth')
const{verify,verifyAdmin} = auth;
const orderControllers = require('../controllers/orderControllers');
const{
createOrder,
retrieveOrders,
retrieveAll,
displayProducts
} = orderControllers;

// createOrder
router.post('/createO',verify,createOrder);

// Retrieve user's orders
router.get('/retrieveO',verify,retrieveOrders);

// Retrieve all orders
router.get('/retrieveAll',verify,verifyAdmin, retrieveAll);

// Display products per order
router.get('/displayP/:id',verify,displayProducts)
module.exports = router;
