const express = require('express');
const router = express.Router();
const auth = require('../auth');
const{verify,verifyAdmin} = auth;
const productControllers = require('../controllers/productControllers');
const {
  createProduct,
  getActiveProducts,
  getSingleProduct,
  updateProductInfo,
  archiveProduct,
  activateProduct,
  searchProduct,
  searchPrice,
  getAllProducts
} = productControllers

// Create Products
router.post('/createP',verify,verifyAdmin,createProduct);

// Retrieve active Products
router.get('/getP', getActiveProducts);

// Get Single Product
router.get('/getSingleP/:id', getSingleProduct);

// Update Product Information
router.put('/updateP/:id',verify,verifyAdmin,updateProductInfo);

// Archive Product
router.put('/archiveP/:id',verify,verifyAdmin,archiveProduct)

// Activate Product
router.put('/activateP/:id',verify,verifyAdmin,activateProduct)

// Search Product
router.post('/searchProd',searchProduct)

// Search by Price
router.post('/searchPrice', searchPrice)

// Get all products Acive and Inactive(admin)
router.get('/allP',verify,verifyAdmin,getAllProducts)
module.exports = router;
