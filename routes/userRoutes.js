const express = require('express'); 
const router = express.Router();
const auth = require('../auth');
const{verify,verifyAdmin} = auth;

const userControllers = require('../controllers/userControllers');
const {
   registerUser,
   loginUser,
   setAdmin,
   removeAdmin,
   getUserDetails
 } = userControllers;

// user Registration
router.post('/register', registerUser);

// user Login
router.post('/login', loginUser);

// set user as an Admin
router.post('/setAdmin', verify, verifyAdmin, setAdmin);

router.post('/removeAdmin', verify, verifyAdmin, removeAdmin);

router.get('/userDetails',verify,getUserDetails);

module.exports = router;
