Project Name: Weapons and Magic Market E-Commerce API

Features:

User Registration
User Authentication
Set User as Admin
Product Creation(Admin Only)
Retrieve all active products
Retrieve single product
Update Product information (Admin only)
Archive Product (Admin only)
Non-admin User checkout (Create Order)
Retrieve authenticated user’s orders
Retrieve all orders (Admin only)
Display Products Per Order (User Only)


Routes and request body:

<!------------------------------- Users --------------------------------------->
<!-- User Registration -->
Post - http://localhost:4000/users/register
Body: (JSON Format)
{
  "firstName": "String",
  "lastName": "String",
  "email": "string",
  "mobileNo": "string"
  "password": "string",
}

<!-- User Login -->
Post - http://localhost:4000/users/login
Body: (JSON Format)
{
  "email": "string",
  "password": "string"
}

<!-- Set User as Admin -->
Get - http://localhost:4000/users/setAdmin
Body: No Request Body
Token: Admin Token Required

<!---------------------------------- Products --------------------------------->

<!-- Create Products -->
Post - http://localhost:4000/products/createP
Body: (JSON Format)
{
  "name": "string",
  "description": "string",
  "price": 100
}

<!-- Get Active Products -->
Get - http://localhost:4000/products/getP
Body: No Request body
Token: No token required

<!-- Get Single Product -->
Get - http://localhost:4000/products/getSingleP/:id
Body: No Request Body
Token: No Token Required

<!-- Update Product Information -->
Put - http://localhost:4000/products/updateP/:id
Body:
{
  "name": "string",
  "description": "string",
  "price": 100
}
Token: Admin Token Required

<!-- Archive Product -->
Put - http://localhost:4000/products/archiveP/:id
Body: No Request Body
Token: Admin Token Required

<!-- Search product by name -->
Post - http://localhost:4000/products/searchProd
Body:
{
  "name": "string"
}
Token: No token required
<!-- Search product by price -->
Post - http://localhost:4000/products/searchPrice
Body:
{
  "price" : 1,
}
Token: No token required
<!------------------------------- Orders -------------------------------------->

<!-- Non-admin User checkout (Create Order)  -->
Post - http://localhost:4000/orders/createO
Body:
{
  "userId": "string",
  "totalAmount": 110000,
  "products": [
    {
      "productId": "string",
      "name": "string",
      "quantity": 1,
      "price": 100000
    },
    {
      "productId": "string",
      "name": "string",
      "quantity": 1,
      "price": 10000
    }
  ]
}

<!-- Retrieve authenticated user’s orders  -->
Get - http://localhost:4000/orders/retrieveO
Body: No Request Body
Token: User Token Required

<!-- Retrieve all Orders -->
Get - http://localhost:4000/orders/retrieveAll

Body: No Request Body
Token: Admin Token Required
