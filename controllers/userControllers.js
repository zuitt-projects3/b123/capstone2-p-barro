const User = require('../models/User');
const bcrypt = require('bcrypt')

const auth = require('../auth');
const{createAccessToken} = auth;

module.exports.registerUser = (req,res) => {
  console.log(req.body);

  User.findOne({email: req.body.email})
  .then(result => {
    if(result !== null && result.email === req.body.email){
      return res.send({message:"Duplicate Email! Register A New Email"});
    } else {
      if(req.body.password.length <8) res.send({message: "Password too Short!"})

      const hashedPW = bcrypt.hashSync(req.body.password,10)
      console.log(hashedPW);

      let newUser = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        password: hashedPW
      })
      newUser.save()
      .then(registeredUser => res.send(registeredUser))
      .catch(err => res.send(err));
    }
  })
.catch(err => res.send(err))
}

module.exports.loginUser = (req,res) => {
  console.log(req.body)
  User.findOne({email: req.body.email})
  .then(result => {
    if(result === null){
      return res.send("No User Found");
    } else {
      const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password)
      console.log(isPasswordCorrect);
      if(isPasswordCorrect){
        console.log('Generate a Key');
        return res.send({accessToken: createAccessToken(result),
        message: "You are now Logged In!"});
      }else{
        return res.send("Password Incorrect"); 
      }
    }
  })
.catch(err => res.send(err))
}



module.exports.setAdmin = (req,res) => {

 User.findById(req.body.ID)
 .then(result => {

  console.log(result)

  if(result.isAdmin === false){

    let updates = {

      isAdmin: true
    }

    User.findByIdAndUpdate(result._id,updates,{new: true})
    .then(admin => res.send({message: `${result.firstName} ${result.lastName} is now an ADMIN!`}))
    .catch(err => res.send(err))
  } else {

    res.send({message: `${result.firstName} ${result.lastName} is already an ADMIN!`})
  }


 })
 .catch(err => res.send(err))
}

module.exports.removeAdmin = (req,res) => {


  User.findById(req.body.ID)
  .then(result => {

    if(result.isAdmin === true){

      let updates = {

        isAdmin: false
      }

      User.findByIdAndUpdate(result._id,updates,{new: true})
      .then(notAdmin => res.send({message: `${result.firstName} ${result.lastName} Admin Status has been removed`}))
      .catch(err => res.send(err))
    } else {

      res.send({message: `${result.firstName} ${result.lastName} is no longer and ADMIN!`})
    }

  })

}

module.exports.getUserDetails = (req,res) => {

  console.log(req.user.id)

  User.findById(req.user.id)

  .then(result => res.send(result))
  .catch(err => res.send(err));

}