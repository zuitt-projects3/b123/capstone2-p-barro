const Order = require('../models/Order');
const Product = require('../models/Product');

module.exports.createOrder = async (req,res) => {

  if(req.user.isAdmin) res.send({
    auth: "Failed",
    message: "Action Forbidden"
  })

    // Check if the body has products
    if(!req.body.products){

      return res.status(400).send({message: "Products Array is required"})
    }

    const hasErrors = req.body.products.find((product)=>{

        if(!product.productId || !product.quantity){

          return true;
        }
    })

    if(hasErrors){

      return res.status(400).send({message: "All Products Require ID and Quantity"})
    }
    // Retieve Products

    const identifier = req.body.products.map((product)=>{

        return product.productId
    })
    let existingProducts
    try{

    existingProducts = await Product.find({_id: {$in: identifier}, isActive: true});
    }
    catch(e){
      console.log(e);
     return res.status(500).send({message: "Try Again Later"})
    }
    // Compute Total 
    console.log(existingProducts);

    const productData = req.body.products.map((product)=>{

      console.log(product);

        const actualProduct = existingProducts.find((p)=>{

            return p.id === product.productId
        })

        console.log(actualProduct);

        const name = actualProduct.name;
        const price = actualProduct.price;
        const quantity = product.quantity;
        const productId = product.productId

        return {name,price,quantity,productId}
    })
    

    const totalAmount = productData.reduce((accumulator, product)=>{
     
      return accumulator = accumulator+(product.price*product.quantity)
    },0)

    console.log(totalAmount)

  let newOrder = new Order ({
  userId: req.user.id,
  totalAmount,
  products: productData
  })

  newOrder.save()
  .then(result => res.send({id: result._id, message: "Order was Successful!"}))
  .catch(err => res.send(err))
  }

module.exports.retrieveOrders = (req,res) => {
  Order.find({userId: req.user.id})
  .then(result => {
    if(req.user.id){
     return res.send(result)
    }

  })
  .catch(err => res.send(err))
}

module.exports.retrieveAll = (req,res) => {
  Order.find({})
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.displayProducts = (req,res) => {
  console.log(req.params.id)
  Order.findById(req.params.id)
  .then(result => res.send(result.products))
  .catch(err => res.send(err))
}
