 const Product = require('../models/Product');

module.exports.createProduct = (req,res) => {
  let newProduct = new Product ({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  })
  newProduct.save()
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.getActiveProducts = (req,res) => {
  Product.find({isActive: true})
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.getSingleProduct = (req,res) => {
  console.log(req.params.id)
  Product.findById(req.params.id)
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.updateProductInfo = (req,res) => {
  console.log(req.params.id)
  let updates = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  }
  Product.findByIdAndUpdate(req.params.id,updates,{new: true})
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.archiveProduct = (req,res) => {
  console.log(req.params.id)
  let updates = {
    isActive: false
  }
  Product.findByIdAndUpdate(req.params.id,updates,{new: true})
  .then(result => res.send({message: "Product is Archived"}))
  .catch(err => res.send(err))
}

module.exports.activateProduct = (req,res) => {
  console.log(req.params.id)
  let updates = {
    isActive: true
  }
  Product.findByIdAndUpdate(req.params.id,updates,{new: true})
  .then(result => res.send({message: "Product is Activated"}))
  .catch(err => res.send(err))
}

module.exports.searchProduct = (req,res) => {
  Product.find({name: req.body.name})
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.searchPrice = (req,res) => {
  Product.find({price: req.body.price})
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.getAllProducts = (req,res) => {

  Product.find({})
  .then(result => res.send(result))
  .catch(err => res.send(err))
}